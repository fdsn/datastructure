<?php
namespace Fdsn\DataStructure;

/**
 * Data structure to handle DateTimw range
 *
 * @param \DateTime	$start	Start date time (from new DateTime() or date_create())
 * @param \DateTime	$end	End date time (from  new DateTime() or date_create())
 */
class DateTimeRange {
	const dateTimeFormat = 'Y-m-d\TH:i:s';
	const dateFormat = 'Y-m-d';
	const iso8601Format = 'c';

	private \DateTime $start;
	private \DateTime $end;

	function __construct( \DateTime $start, \DateTime $end) {
		$this->start = $start <= $end ? $start : $end;
		$this->end = $start > $end ? $start : $end;
	}

	/**
	 * Get start date time
	 * @return string 	Start date time in format YYYY-mm-ddTHH:mm:ss'
	 */
	public function startDateTime():string { return date_format($this->start, self::dateTimeFormat); }

	/**
	 * Get end date time
	 * @return string 	End date time in format YYYY-mm-ddTHH:mm:ss'
	 */
	public function endDateTime():string { return date_format($this->end, self::dateTimeFormat); }


	/**
	 * Get start date 
	 * @return string 	Start date in format YYYY-mm-dd'
	 */
	public function startDate():string { return date_format($this->start, self::dateFormat); }

	/**
	 * Get end date 
	 * @return string 	End date in format YYYY-mm-dd'
	 */
	public function endDate():string { return date_format($this->end, self::dateFormat); }

	/**
	 * Get start date time
	 * @return string 	Start date time in format ISO 8601 (see: https://www.php.net/manual/it/datetime.format.php )
	 */
	public function startISO8601():string { return date_format($this->start, self::iso8601Format); }

	/**
	 * Get end date time
	 * @return string 	End date time in format ISO 8601  (see: https://www.php.net/manual/it/datetime.format.php )
	 */
	public function endISO8601():string { return date_format($this->end, self::iso8601Format); }

} 




?>
