<?php
namespace Fdsn\DataStructure;

/**
 * Data structure to handle Location
 *
 * @param string $name		Name of location
 *
 * @return true, if every check is passed, false otherwise
 */

class Location {
	private string $name;

	function __construct(string $name) {

		if( empty($name) )
			throw new \InvalidArgumentException("Location name invalid");

		$this->name = $name;
	}

	function __destruct(){ }

	/**
	 * Returns location 
	 *
	 * @return string Location name
	 */
	function __toString(){ return sprintf("%s", $this->name); }
} 




?>
