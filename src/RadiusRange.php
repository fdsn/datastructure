<?php
namespace Fdsn\DataStructure;

/**
 * Data structure to handle Radius range
 *
 * @param Radius $min	Min radius
 * @param Radius $max	Max radius
 */
class RadiusRange {
	private Radius $min;
	private Radius $max;

	function __construct( Radius $min, Radius $max) {
		$this->min = $min->value() <= $max->value() ? $min : $max;
		$this->max = $min->value() > $max->value() ? $min : $max;
	}

	function __destruct(){ }

	/**
	 * Returns bounding box coordinates 
	 *
	 * @return string Radius range in [%.2f,%.2f] [min radius, max radius] format
	 */
	function __toString(){ 
		return sprintf("[%.2f,%.2f]",
			$this->min->value(), 
			$this->max->value()); 
	}

	/**
	 * Get min radius
	 *
	 * @return Radius  Min radius
	 */
	public function min():Radius { return $this->min; }

	/**
	 * Get max radius
	 *
	 * @return Radius  Max radius
	 */
	public function max():Radius { return $this->max; }

} 

?>
