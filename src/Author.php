<?php
namespace Fdsn\DataStructure;

/**
 * Data structure to handle Author
 *
 * @param string $name		Name of author
 */
class Author {
	private string $name;

	function __construct(string $name){

		if( empty($name) )
			throw new \InvalidArgumentException("Author name invalid");

		$this->name = $name;
	}

	function __destruct(){ }

	function __toString(){ return sprintf("%s", $this->name); }

	/**
	 * Get Catalog name
	 * @return string Catalog name
	 */
	public function name():string { return $this->name;}
} 
?>
