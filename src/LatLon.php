<?php
namespace Fdsn\DataStructure;

/**
 * Data structure to handle Latitude,Longitude point
 *
 * @param float $latitude		Latitude (-180~180)
 * @param float $longitude		Longitude (-180~180) 
 *
 * @return true, if every check is passed, false otherwise
 */
class LatLon {
	private float $lat;
	private float $lon;

	/**
	 * Range of data validity
	 * @see	https://www.fdsn.org/webservices/fdsnws-event-1.2.pdf FDSN official documentation
	 */
	private $validLatitude = array('min' => -90, 'max' => 90);

	/**
	 * Range of data validity
	 * @see	FDSN Doc https://www.fdsn.org/webservices/fdsnws-event-1.2.pdf
	 */
	private $validLongitude = array('min' => -180, 'max' => 180);

	function __construct( float $latitude, float $longitude) {

		if( ! $this->isValidLatitude($latitude) )
			throw new \InvalidArgumentException("Latitude invalid");

		if (! $this->isValidLongitude($longitude)) 
			throw new \InvalidArgumentException("Longitude invalid");

		$this->lat = $latitude;
		$this->lon = $longitude;
	}

	function __destruct(){ }

	function __toString(){ return sprintf("%.5f,%.5f", $this->lat, $this->lon); }

	/**
	 * Get latitude
	 *
	 * @return float latitude
	 */
	public function lat():float { return $this->lat; }

	/**
	 * Get longitude
	 *
	 * @return float longitude
	 */
	public function lon():float { return $this->lon; }

	/**
	 * Check latitude validity
	 *
	 * @return bool True if is valid, false otherwise
	 */
	private function isValidLatitude(float $latitude){ return ($this->validLatitude['min'] <= $latitude && $latitude <= $this->validLatitude['max']); }

	/**
	 * Check longitude validity
	 *
	 * @return bool True if is valid, false otherwise
	 */
	private function isValidLongitude(float $longitude){ return ($this->validLongitude['min'] <= $longitude && $longitude <= $this->validLongitude['max']); }

} 

?>
