<?php
namespace Fdsn\DataStructure;

/**
 * Data structure to handle Contributor
 *
 * @param string $name		Name of catalog
 */
class Contributor {
	private string $name;

	function __construct(string $name){

		if( empty($name) )
			throw new \InvalidArgumentException("Contributor name invalid");

		$this->name = $name;
	}

	function __destruct(){ }

	function __toString(){ return sprintf("%s", $this->name); }

	/**
	 * Get Contributor name
	 * @return string Contributor name
	 */
	public function name():string { return $this->name;}
} 
?>
