<?php
namespace Fdsn\DataStructure;

/**
 * Data structure to handle Latitude,Longitude bounding box
 *
 * @param LatLon $min	Min Latitude,Longitude 
 * @param LatLon $max	Max Latitude,Longitude 
 */
class LatLonRange {
	private LatLon $min;
	private LatLon $max;

	private $validLatitude = array('min' => -180, 'max' => 180);
	private $validLongitude = array('min' => -180, 'max' => 180);

	function __construct( LatLon $min, LatLon $max) {
		$this->min = $min;
		$this->max = $max;
	}

	function __destruct(){ }

	/**
	 * Returns bounding box coordinates 
	 *
	 * @return string Bounding box in [%.5f,%.5f],[%.5f,%.5f] [min lat,lon],[max lat,lon] format
	 */
	function __toString(){ 
		return sprintf("[%.5f,%.5f],[%.5f,%.5f]", 
			$this->min->lat(), $this->min->lon(),
			$this->max->lat(), $this->max->lon()); 
	}

	/**
	 * Get min bouding box coordinates
	 *
	 * @return LatLon  Min bouding box coordinates
	 */
	public function min():LatLon { return $this->min; }

	/**
	 * Get max bouding box coordinates
	 *
	 * @return LatLon  Max bouding box coordinates
	 */
	public function max():LatLon { return $this->max; }
} 

?>
