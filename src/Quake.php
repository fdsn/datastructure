<?php
namespace Fdsn\DataStructure;

use Fdsn\DataStructure\Magnitude;
use Fdsn\DataStructure\Epicenter;
use Fdsn\DataStructure\Location;
use Fdsn\DataStructure\Author;

/**
 * Data structure to handle information about earthquakes
 * This object must be instantiated only with quake data
 * 
 * @param int $eventId		EventID according to INGV ONT ID (see https://terremoti.ingv.it/)
 * @param \DateTime|null	$originTimeUTC		Quake UTC origin time, in ISO 8601 format (see: date('c'))
 * @param Location|null 	$location		Where the quake origin is
 * @param Magnitude|null 	$magnitude		Magnitude obj
 * @param Epicenter|null 	$epicenter 		Epicenter obj
 * @param Author|nul	l 	$author			Author obj
 */
class Quake {
	private int $eventId;
	private \DateTime $originTimeUTC;
	private Location  $location;
	private Magnitude $magnitude;
	private Epicenter $epicenter;
	private Author $author;

	function __construct(
		int $eventId, 
		?\DateTime $originTimeUTC = null, 
		?Location  $location = null, 
		?Magnitude $magnitude = null, 
		?Epicenter $epicenter = null,
		?Author	$author = null) {

		if( (is_null($eventId) || 0 == $eventId) )
			throw new \InvalidArgumentException("Event ID unset");
		
		if ( '' == $originTimeUTC )
			throw new \InvalidArgumentException("originTime unset");

		$this->eventId = $eventId;
		$this->originTimeUTC = $originTimeUTC;
		$this->location = $location;
		$this->magnitude = $magnitude;
		$this->epicenter = $epicenter;
		$this->author = $author;
	}

	function __destruct(){ }

	/**
	 * Returns quake details
	 *
	 * @return string Quake details: eventId, originTimeUTC, magnitude, epicenter, location
	 */
	function __toString(){
		return sprintf("Quake ID %d, on %s, mag: %s, origin: %s, location: %s",
			$this->eventId, 
			$this->originTimeUTC, 
			! is_null($this->magnitude) ? $this->magnitude : '', 
			! is_null($this->epicenter) ? $this->epicenter : '', 
			! is_null($this->location) ? $this->location : ''
		);
	}

	/**
	 * Get event ID
	 *
	 * @return int Event ID
	 */
	public function eventId():int { return $this->eventId; }

	/**
	 * Get UTC origin time
	 *
	 * @return int UTC Origin time in YYYY-mm-ddTHH:mm:ss
	 */
	public function originTimeUTC():string { return $this->originTimeUTC->format('Y-m-d\TH:m:s'); }

	/**
	 * Get UTC origin time
	 *
	 * @return int UTC Origin time in YYYY-mm-ddTHH:mm:ss
	 */
	public function originTimeUTCISO8601():string { return $this->originTimeUTC->format('c'); }

	/**
	 * Get magnitude
	 *
	 * @return Magnitude Magnitude obj
	 */
	public function magnitude():Magnitude { return $this->magnitude; }

	/**
	 * Get epicenter
	 *
	 * @return Epicenter Epicenter obj
	 */
	public function epicenter():Epicenter { return $this->epicenter; }

	/**
	 * Get location
	 *
	 * @return Location Location obj
	 */
	public function location():Location { return $this->location; }

	/**
	 * Get author
	 *
	 * @return Author Author obj
	 */
	public function author():Author { return $this->author; }

}
?>
