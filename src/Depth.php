<?php
namespace Fdsn\DataStructure;

/**
 * Data structure to handle Depth value
 *
 * @param float $value 	depth
 */
class Depth {
	private float $value;

	private $validValues = array('min' => -INF, 'max' => INF);

	function __construct( float $value) {

		if( ! $this->isValidValue($value) )
			throw new \InvalidArgumentException("Depth value invalid");

		$this->value = $value;
	}

	function __destruct(){ }

	/**
	 * Get depth value
	 * @return string 	Depth value in "%.2f km" format
	 */
	function __toString(){ return sprintf("%.2f km", $this->value); }

	/**
	 * Get depth value
	 * @return float	Depth value
	 */
	public function value():float { return $this->value; }

	/**
	 * Check if depth value is valid
	 * @return bool 	True if is valid, false otherwise
	 */
	private function isValidValue(float $value):bool { return ($this->validValues['min'] <= $value && $value <= $this->validValues['max']); }
} 




?>
