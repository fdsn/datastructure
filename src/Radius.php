<?php
namespace Fdsn\DataStructure;

/**
 * Data structure to handle Latitude,Longitude point
 *
 * @param float $latitude		Latitude (-180~180)
 * @param float $longitude		Longitude (-180~180) 
 *
 * @return true, if every check is passed, false otherwise
 */
class Radius {
	private float $value;

	private $validRadius = array('min' => 0, 'max' => 180);

	function __construct( float $value) {

		if( ! $this->isValid($value) )
			throw new \InvalidArgumentException("Radius invalid");

		$this->value = $value;
	}

	function __destruct(){ }

	function __toString(){ return sprintf("%.2f", $this->value); }

	/**
	 * Get radius value
	 *
	 * @return float radius
	 */
	public function value():float { return $this->value; }

	/**
	 * Check radius validity
	 *
	 * @return bool True if is valid, false otherwise
	 */
	private function isValid(float $value){ return ($this->validRadius['min'] <= $value && $value <= $this->validRadius['max']); }
} 

?>
