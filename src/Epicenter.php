<?php
namespace Fdsn\DataStructure;

use Fdsn\DataStructure\LatLon;
use Fdsn\DataStructure\Depth;

/**
 * Data structure to handle epicenters
 * 
 * @param LatLon $point 		LatLon obj
 * @param Depth $depth			Depth obj
 *
 * @return true, if every check is passed, false otherwise
 */
class Epicenter {
	private LatLon $point;
	private Depth $depth;

	function __construct( LatLon $point, Depth $depth) {

		if( ( is_null($point) || ! $point instanceof LatLon) )
			throw new \InvalidArgumentException("Point unset or not instanceof LatLon");
			
		$this->point = $point;
		$this->depth = $depth;
	}

	function __destruct(){ }

	/**
	 * Returns epicenter in lat, lon, depth readable format
	 *
	 * @return string  lat, lon, depth ("%.5f,%.5f, %.2f km")
	 */
	function __toString(){
		return sprintf("%s, %s", 
			$this->point, 
			$this->depth);
	}

	/**
	 * Get epicenter coordinates
	 *
	 * @return LatLon 	epicenter coordinates
	 */
	public function point():LatLon { return $this->point; }

	/**
	 * Get epicenter depth
	 *
	 * @return Depth 	epicenter depth
	 */
	public function depth():Depth { return $this->depth; }
} 




?>
