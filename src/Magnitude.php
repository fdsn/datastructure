<?php
namespace Fdsn\DataStructure;

/**
 * Data structure to handle magnitude type and value
 *
 * @param string $magnitudeType 	Magnitude type 
 * @param float $magnitudeValue		Magniude value
 *
 * @return true, if every check is passed, false otherwise
 */

class Magnitude {
	private string $type;
	private float $value;

	private $validTypes = array('ml', 'md', 'mb', 'ms', 'mw', 'mwp');
	private $validValues = array('min' => -12, 'max' => 12);

	function __construct( string $type, float $value) {

		if( empty($type) )
			throw new \InvalidArgumentException("Magnitude type unset or empty string");
	
		if( ! $this->isValidType($type) )
			throw new \InvalidArgumentException("Magnitude type invalid");

		if( ! $this->isValidValue($value) )
			throw new \InvalidArgumentException("Magnitude value invalid");

		$this->type = $type;
		$this->value = $value;
	}

	function __destruct(){ }

	/**
	 * Returns magnitude
	 *
	 * @return string Magnitude type and value in "%s %.2f" format
	 */
	function __toString(){ return sprintf("%s %.2f", $this->type, $this->value); }

	/**
	 * Get magnitude value
	 *
	 * @return float Magnitude value
	 */
	public function value():float{ return $this->value; }

	/**
	 * Get magnitude type
	 *
	 * @return string 	Magnitude type
	 */
	public function type():string{ return $this->type; }

	/**
	 * Get magnitude type in lower case
	 *
	 * @return string 	Magnitude type in lowercase
	 */
	public function typeLowerCase():string{ return strtolower($this->type); }

	/**
	 * Check is type is supported
	 *
	 * @return bool  True is supported, false otherwise
	 */
	private function isValidType(string $type){ return in_array(strtolower($type), $this->validTypes); } 

	/**
	 * Check is value is valid 
	 *
	 * @return bool  True is valid, false otherwise
	 */
	private function isValidValue(float $value){ return ($this->validValues['min'] <= $value && $value <= $this->validValues['max']); }
} 




?>
