<?php
namespace Fdsn\DataStructure;

/**
 * Data structure to handle magnitude range
 *
 * @param Magnitude $min	Min magnitude
 * @param Magnitude $max	Max magnitude
 *
 * @return true, if every check is passed, false otherwise
 */

class MagnitudeRange {
	private Magnitude $min;
	private Magnitude $max;

	function __construct( Magnitude $min, Magnitude $max) {

		$this->min = $min->value() <= $max->value() ? $min : $max;
		$this->max = $min->value() > $max->value() ? $min : $max;
	}

	/**
	 * Get min magnitude obj
	 *
	 * @return Magnitude get min Magnitude obj
	 */
	public function min():Magnitude { return $this->min; }

	/**
	 * Get max magnitude obj
	 *
	 * @return Magnitude get max Magnitude obj
	 */
	public function max():Magnitude { return $this->max; }
} 




?>
