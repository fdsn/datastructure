<?php
namespace Fdsn\DataStructure;

/**
 * Data structure to handle depth range.
 * If min > max, args are swapped
 *
 * @param Depth $min	Min depth
 * @param Depth $max	Max depth
 *
 */

class DepthRange {
	private Depth $min;
	private Depth $max;

	function __construct( Depth $min, Depth $max) {

		$this->min = $min->value() <= $max->value() ? $min : $max;
		$this->max = $min->value() > $max->value() ? $min : $max;
	}

	/**
	 * Get min depth
	 *
	 * @return Depth  Get min-Depth obj 
	 */
	public function min():Depth { return $this->min; }

	/**
	 * Get max depth
	 *
	 * @return Depth  Get max-Depth obj 
	 */
	public function max():Depth { return $this->max; }
} 




?>
