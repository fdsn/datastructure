<?php
namespace Fdsn\DataStructure;

/**
 * Data structure to handle Catalog
 *
 * @param string $name		Name of catalog
 */
class Catalog {
	private string $name;

	function __construct(string $name){

		if( empty($name) )
			throw new \InvalidArgumentException("Catalog name invalid");

		$this->name = $name;
	}

	function __destruct(){ }

	function __toString(){ return sprintf("%s", $this->name); }

	/**
	 * Get Catalog name
	 * @return string Catalog name
	 */
	public function name():string { return $this->name;}
} 
?>
