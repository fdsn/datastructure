# FDSN Event dataStructure

This library is a collection of data structures used by [FDSN/webservice-library](https://packagist.org/packages/fdsn/webservice-library).


## Install library

### Via composer 
```sh
composer require fdsn/datastructure
``` 
Include library directly via _autoload_:
```php
require_once __DIR__ . '/../vendor/autoload.php';
```

### Manually
Clone the following repositories: 
- https://gitlab.rm.ingv.it/fdsn/datastructure.git

and manually adjust the path

Include libraries:
```php
require_once <install_dir>/datastructure/src/<all_files>.php
```

## Usage
Add namespace (__replace "<className_you_need_to_use>" with the class name you need__):
```php
use Fdsn\DataStructure\<className_you_need_to_use> as Fdsnws_Classname;
```

create object (__replace args with the arguments required by the class__):
```php
$obj = new Fdsnws_Classname(<args>);
``` 
and use its methods.

## Documentation
You can find documentation, automatically created by PHPDocumentor here: http://fdsn.gitpages.rm.ingv.it/datastructure/

## Test and Deploy
Tests are realized using PHPUnit.


## Contributing
If you want to contribute, please use pull requests.
To get a best integration, please code using Behavior Driven Development (BDD), Test Driven Development (TDD) and Calisthenic Programming.

## Authors and acknowledgment
Diego Sorrentino, Istituto Nazionale di Geofisica e Vulcanologia, https://www.ingv.it/organizzazione/chi-siamo/personale/#922

## License
GPL v3

## Project status
Development in progress
