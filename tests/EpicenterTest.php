<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\LatLon;
use Fdsn\DataStructure\Depth;
use Fdsn\DataStructure\Epicenter;

class EpicenterTest extends TestCase{
	public static function dataProvider(): array{
		return [
			[new LatLon(10,10), new Depth(5)],
			[new LatLon(90,150), new Depth(50)]
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewEpicenter(LatLon $point, Depth $depth): void{
		$obj = new Epicenter($point, $depth);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\Epicenter", $obj);
	}
}



?>

