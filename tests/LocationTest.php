<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\LatLon;
use Fdsn\DataStructure\Location;

class LocationTest extends TestCase{
	public static function dataProvider(): array{
		return [
			['Roma']
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewLocation(string $name): void{
		$obj = new Location($name);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\Location", $obj);

	}
}



?>

