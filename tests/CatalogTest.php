<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\Catalog;

class CatalogTest extends TestCase{
	public static function dataProvider(): array{
		return [
			['Pippo']
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewCatalog(string $name): void{
		$obj = new Catalog($name);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\Catalog", $obj);
		
		$this->assertSame($name, $obj->name());

	}
}



?>

