<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\Radius;
use Fdsn\DataStructure\RadiusRange;

class RadiusRangeTest extends TestCase{
	public static function dataProvider(): array{
		return [
			[ new Radius(5), new Radius(10) ],
			[ new Radius(8),   new Radius(21) ],
			[ new Radius(100), new Radius(5.7) ],
			[ new Radius(1.4), new Radius(9) ],
			[ new Radius(0.9), new Radius(10) ]
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewRadiusRange(Radius $min, Radius $max): void{
		$obj = new RadiusRange($min, $max);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\RadiusRange", $obj);

		//NOTE: in assertLessThanOrEqual() values are swapped, respect to my thought
		// https://phpunit.de/manual/6.5/en/appendixes.assertions.html#appendixes.assertions.assertLessThanOrEqual
		// assertLessThanOrEqual(mixed $expected, mixed $actual[, string $message = ''])
		// Reports an error identified by $message if the value of $actual is not less than or equal to the value of $expected.
		$this->assertLessThanOrEqual($obj->max()->value(), $obj->min()->value());
	}
}



?>

