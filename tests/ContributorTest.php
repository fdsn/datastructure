<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\Contributor;

class ContributorTest extends TestCase{
	public static function dataProvider(): array{
		return [
			['Pippo']
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewContributor(string $name): void{
		$obj = new Contributor($name);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\Contributor", $obj);
		
		$this->assertSame($name, $obj->name());

	}
}



?>

