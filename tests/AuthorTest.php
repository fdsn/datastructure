<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\Author;

class AuthorTest extends TestCase{
	public static function dataProvider(): array{
		return [
			['Pippo']
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewAuthor(string $name): void{
		$obj = new Author($name);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\Author", $obj);
		
		$this->assertSame($name, $obj->name());

	}
}



?>

