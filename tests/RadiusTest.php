<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\Radius;

class RadiusTest extends TestCase{
	public static function dataProvider(): array{
		return [
			[10],
			[140.20]
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewRadius(float $value): void{
		$obj = new Radius($value);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\Radius", $obj);
		$this->assertEquals($value, $obj->value());

	}
}



?>
