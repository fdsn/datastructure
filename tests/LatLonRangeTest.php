<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\LatLon;
use Fdsn\DataStructure\LatLonRange;

class LatLonRangeTest extends TestCase{
	public static function dataProvider(): array{
		return [
			[ new LatLon(28,12), new LatLon(31,13) ],
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewLatLonRange(LatLon $min, LatLon $max): void{
		$obj = new LatLonRange($min, $max);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\LatLonRange", $obj);
	}
}



?>

