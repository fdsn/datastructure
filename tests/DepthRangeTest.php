<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\Depth;
use Fdsn\DataStructure\DepthRange;

class DepthRangeTest extends TestCase{
	public static function dataProvider(): array{
		return [
			[ new Depth(-15), new Depth(10) ],
			[ new Depth(8),   new Depth(21) ],
			[ new Depth(100), new Depth(5.7) ],
			[ new Depth(1.4), new Depth(9) ],
			[ new Depth(0.9), new Depth(10) ]
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewDepthRange(Depth $min, Depth $max): void{
		$obj = new DepthRange($min, $max);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\DepthRange", $obj);

		//NOTE: in assertLessThanOrEqual() values are swapped, respect to my thought
		// https://phpunit.de/manual/6.5/en/appendixes.assertions.html#appendixes.assertions.assertLessThanOrEqual
		// assertLessThanOrEqual(mixed $expected, mixed $actual[, string $message = ''])
		// Reports an error identified by $message if the value of $actual is not less than or equal to the value of $expected.
		$this->assertLessThanOrEqual($obj->max()->value(), $obj->min()->value());
	}
}



?>

