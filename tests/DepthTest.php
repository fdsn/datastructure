<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\Depth;

class DepthTest extends TestCase{
	public static function dataProvider(): array{
		return [
			[-15],
			[8],
			[5.7],
			[1.4],
			[0.9],
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewDepth(float $value): void{
		$obj = new Depth($value);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\Depth", $obj);
		$expected = sprintf("%.2f km", $value);
		$this->assertEquals($expected, $obj);

	}
}



?>

