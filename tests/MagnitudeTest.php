<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\Magnitude;

class MagnitudeTest extends TestCase{
	public static function dataProvider(): array{
		return [
			['md', 5],
			['ml', 8],
			['mw', 5],
			['mwp', 1],
			['mb', 0],
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewMagnitude(string $type, float $value): void{
		$obj = new Magnitude($type, $value);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\Magnitude", $obj);
		$expected = sprintf("%s %.2f", $type, $value);
		$this->assertEquals($expected, $obj);

	}
}



?>

