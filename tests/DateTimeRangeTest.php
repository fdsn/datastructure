<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\DateTimeRange;

class DateTimeRangeTest extends TestCase{
	public static function dataProvider(): array{
		return [
			[new \DateTime('2021-10-30T10:00:01'), new \DateTime('2021-12-30T10:00:01')]
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewDateTimeRange(\DateTime $startTime, \DateTime $endTime): void{
		$obj = new DateTimeRange($startTime, $endTime);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\DateTimeRange", $obj);

		$this->assertEquals($startTime->format(DateTimeRange::dateTimeFormat), $obj->startDateTime());
		$this->assertEquals($endTime->format(DateTimeRange::dateTimeFormat), $obj->endDateTime());

		$this->assertEquals($startTime->format(DateTimeRange::dateFormat), $obj->startDate());
		$this->assertEquals($endTime->format(DateTimeRange::dateFormat), $obj->endDate());

		$this->assertEquals($startTime->format(DateTimeRange::iso8601Format), $obj->startISO8601());
		$this->assertEquals($endTime->format(DateTimeRange::iso8601Format), $obj->endISO8601());

	}
}



?>

