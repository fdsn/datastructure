<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\LatLon;

class LatLonTest extends TestCase{
	public static function dataProvider(): array{
		return [
			[10, 10],
			[89, 100.29]
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewLatLon(float $lat, float $lon): void{
		$obj = new LatLon($lat, $lon);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\LatLon", $obj);
		$expected = sprintf("%.5f,%.5f", $lat, $lon);
		$this->assertEquals($expected, $obj);

	}
}



?>
