<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Fdsn\DataStructure\Quake;
use Fdsn\DataStructure\Magnitude;
use Fdsn\DataStructure\LatLon;
use Fdsn\DataStructure\Depth;
use Fdsn\DataStructure\Epicenter;
use Fdsn\DataStructure\Location;
use Fdsn\DataStructure\Province;
use Fdsn\DataStructure\Author;

class QuakeTest extends TestCase{
	public static function dataProvider(): array{
		return [
			[	12345, 
				new \Datetime('2004-02-12T15:19:21+00:00'), 
				new Location('Da qualche parte'),
				new Magnitude('mw', 5), 
				new Epicenter(new LatLon(10,10), new Depth(5)),
				new Author('bollettino')
			]
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewQuake(int $id, \Datetime $originTimeUTC, Location $location, Magnitude $magnitude, Epicenter $epicenter, Author $author): void{
		$obj = new Quake($id, $originTimeUTC, $location, $magnitude, $epicenter, $author);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\Fdsn\\DataStructure\\Quake", $obj);
	}
}



?>

